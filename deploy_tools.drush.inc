<?php

/**
 * Implements hook_drush_command().
 *
 * Register the typeinfo command(s)
 */
function deploy_tools_drush_command() {
  $commands = array();

  $commands['deploy-tools-content-type-dependencies'] = array(
    'callback' => 'drush_deploy_tools_content_type_dependencies',
    'description' => 'Show all the dependencies that the specific content type needs. Helpful when content type needs to be deployed',
    'aliases' => array('dt-ct-dep', 'dt-ctd'),
    'examples' => array(
      'drush deploy-tools-content-type-dependencies [content type]' => 'List all the dependencies needed for this content type',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'content_type' => 'Content type to provide the dependencies',
    ),
    'options' => array(
      'format' => 'output format. Options: php (Default. Php print_r), table, list, yaml, json, xml',
      'depth' => 'how many layers the program descends. Default: 10',
    ),
  );

  return $commands;
}

/*
 * callback for content type dependencies
 */
function drush_deploy_tools_content_type_dependencies($content_type) {

  $format = drush_get_option('format','php');
  $depth = drush_get_option('depth',10);

  /*
   * call the api function
   */
  $output = _deploy_tools_content_type_dependencies($content_type,$depth);

  /*
   * print output
   */
  switch ($format) {
    case 'table':
      drush_print("Content type " + $content_type + " dependencies list");
      drush_print("");
      /*
       * 1) modules
       *
       * prepare rows
       */
      $rows = array();
      $c1 = strlen('Module');
      // insert headers
      $rows[] = array("Module","Fields requiring this module");
      foreach ($output['dependencies']['modules'] as $module => $fields) {
        // insert data row
        $rows[] = array(
          $module,
          join(" , ",array_keys($fields)));
        // define first column width
        $c1 = max($c1, strlen($module));
      };
      // print table
      drush_print_table($rows,TRUE,array($c1,NULL));
      drush_print("");
      /*
       * 2) taxonomies
       */
      $rows = array();
      $c1 = strlen('Taxonomy');
      $rows[] = array("Taxonomy","Fields using this taxonomy");
      foreach ($output['dependencies']['taxonomies'] as $taxonomy => $fields) {
        $rows[] = array(
          $taxonomy,
          join(" , ",array_keys($fields)));
        $c1 = max($c1, strlen($taxonomy));
       
      };
      drush_print_table($rows,TRUE,array($c1,NULL));
      drush_print("");
      /*
       * 3) views
       */
      $rows = array();
      $c1 = strlen('View');
      $c2 = strlen('Display');
      $rows[] = array("View","Display","Fields using this view and display");
      foreach ($output['dependencies']['views'] as $view => $displays) {
        foreach ($displays as $display => $fields) {
          $rows[] = array(
            $view,
            $display,
            join(" , ",array_keys($fields)));
          $c2 = max($c2,strlen($display));
        };
        $c1 = max($c1,strlen($view));
      };
      drush_print_table($rows,TRUE,array($c1,$c2,NULL));
      drush_print("");
      /*
       * 4) filed collections
       */
      $rows = array();
      $c1 = strlen('Field Collection');
      $rows[] = array("Field Collection","Fields contained in this field collection");
      foreach ($output['dependencies']['field_collections'] as $fc => $fields) {
        $rows[] = array(
          $fc,
          join(" , ",array_keys($fields)));
        $c1 = max($c1,strlen($fc));
      };
      drush_print_table($rows,TRUE);
      drush_print("");
      /*
       * 5) content types
       */
      $rows = array();
      $c1 = strlen('Content Types');
      $rows[] = array("Content Types","Fields referring to this content type");
      foreach ($output['dependencies']['content_types'] as $fc => $fields) {
        $rows[] = array(
          $fc,
          join(" , ",array_keys($fields)));
        $c1 = max($c1,strlen($fc));
      };
      drush_print_table($rows,TRUE);
      drush_print("");


      break;
    case 'list':
      // translation tables
      $ttType = array(
         'modules' => 'M',
         'taxonomies' => 'TX',
         'views' => 'V',
         'display' => 'VD',
         'field_collections' => 'FC',
         'content_types' => 'CT' );
      // initialize rows
      $rows = array();
      // loop on the type
      foreach ($output['dependencies'] as $type => $list1) {
        // define extra info custom for each type
        // if null, skip
        $extras = array();
        foreach ($list1 as $name1 => $list2) {
          // append name
          $rows[] = array($ttType[$type] . ":" . $name1);
          // extras
          switch ($type) {
            case 'views':
              // add view's displays
              foreach ($list2 as $name2 => $list3) {
                $extras[] = array($ttType['display'] . ":" .$name1 . ":" . $name2);
              }
              break;
          }
        }
        // insert extras
        if ($extras) {
          $rows = array_merge($rows,$extras);
        }
      };
      drush_print_table($rows);
      break;
    case 'php':
    default:
      drush_print(print_r($output['dependencies']));
      break;
  }

}


/*
 * load and package all the info for the fields for the entity requested
 */
function _deploy_tools_fields_info($bundle, $entity) {
  /*
   * load field instances for the content type
   */
  $instances = field_info_instances($bundle,$entity);
  /*
   * load fields info for this content type
   */
  $fields = array();
  /*
   * loop on all the fields loaded
   */
  foreach ( $instances as $name => $instance) {
    // load info for the field
    $field = field_info_field($name);
    // first join instances and field info;
    $fields[$name] = array(
      'instance' => $instance,
      'field' => $field );
  }
  return $fields;
}



/*
 * private function to find dependencies for the content type
 */
function _deploy_tools_content_type_dependencies($content_type,$depth = 10) {

  /*
   * load fields info
   */
  $fields = _deploy_tools_fields_info('node',$content_type);

  /*
   * compile content type dependencies
   */
  $dependencies = array();

  /*
   * counter to avoid inifite loop
   */
  // loops counter
  $cl = 0;
  // maximum loops
  $ml = $depth;

  /*
   * keep looping if we have fields
   */
  while ($fields) {

    /*
     * list of fields checked and to be check in the next round
     */
    $completefields = array();
    $nextfields = array();
    /*
     * loops on every field
     */
    foreach ($fields as $name => $info) {

      /*
       * now we go for the field dependencies
       *
       * first we get the module from the field and the module required from the widget saelected
       */
      drupal_array_set_nested_value($dependencies, array('modules', $info['field']['module'], $name), $name);
      drupal_array_set_nested_value($dependencies, array('modules', $info['instance']['widget']['module'], $name), $name);
      /*
       * now we do the dirty work based on the type o field
       * eventually we should transfer this in a plugin system, so other modules can add their own callback
       */
      switch($info['field']['type']) {
        case "viewfield":
          /*
           * field type: viewfield
           */
          // first add field to views module
          //drupal_array_set_nested_value($dependencies, array('modules','views',$name),$name);
          // insert module viewfield
          drupal_array_set_nested_value($dependencies, array('modules','viewfield', $name), $name);
          // check settings
          if ( drupal_array_get_nested_value($info,array('instance','settings','force_default')) ) {
            // insert only default
            // get view and display name from default value
            list($view, $display) = explode('|', 
              drupal_array_get_nested_value($info,array('instance','default_value',0,'vname')));
            // insert view and display
            drupal_array_set_nested_value($dependencies, array('views', $view, $display, $name), $name);
          }
          break;
        case "taxonomy_term_reference":
          // module taxonomy
          //drupal_array_set_nested_value($dependencies, array('modules', 'taxonomy', $name), $name);
          // insert taxonomies
          foreach (
              drupal_array_get_nested_value($info,array('field','settings','allowed_values')) as $item ) {
            // insert taxonomy
            drupal_array_set_nested_value($dependencies, 
              array(
                'taxonomies', 
                $item['vocabulary'],
                $name),
              $name);
          }
          break;
        case "field_collection":
          // module
          //drupal_array_set_nested_value($dependencies, array('modules', 'field_collection', $name), $name);
          // load field collection entity and list fields to check next round
          $fcfields = _deploy_tools_fields_info('field_collection_item',$name);
          foreach ($fcfields as $fname => $fitem) {
            // field collection field and entity
            drupal_array_set_nested_value($dependencies, array('field_collections', $name, $fname), $fname);
          }
          // load field collection entity and list fields to check next round
          $nextfields += $fcfields;
          break;
        case "entityreference":
          // module
          //drupal_array_set_nested_value($dependencies, array('modules', 'entityreference', $name), $name);
          // content type
          foreach (
              drupal_array_get_nested_value($info,array('field','settings','handler_settings','target_bundles')) as $ctname => $item ) {
            // insert content type
            drupal_array_set_nested_value($dependencies, array('content_types', $ctname, $name), $name);
            // load fields of this content type
            $ctfields = _deploy_tools_fields_info(
                            drupal_array_get_nested_value($info,array('field','settings','target_type')),
                            $name);
            // add fields in list to check next round
            $nextfields += $ctfields;
          }
          break;
        case "datetime":
        default:
//          drupal_array_set_nested_value($dependencies, array('modules', $info['field']['module'], $name), $name);
          break;
      }
    }
    /*
     * transfer new field list
     */
    $completefields += $fields;
    $fields = $nextfields;

    /*
     * check if we are done
     */
    $cl += 1;
    if ( $cl >= $ml )
      break;
  }

  return array(
    'fields' => $completefields,
    'dependencies' => $dependencies
  );
}

